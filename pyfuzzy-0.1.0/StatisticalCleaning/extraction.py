import sqlite3,collections,pickle
conn = sqlite3.connect('strategy.sqlite')
def dd():
	return collections.defaultdict(list)
crop_companion = collections.defaultdict(dd)
crop_strategy = collections.defaultdict(list)
mc = conn.cursor()
mc.execute("SELECT label,evidence_candidate_id,relation_id  FROM corpus_evidencelabel")
while True:
	mrow = mc.fetchone()
	if mrow == None:
		break
	if mrow[0] != 'YE':
		continue
	cid = mrow[1]
	c = conn.cursor()
	c.execute("SELECT left_entity_occurrence_id,right_entity_occurrence_id,segment_id FROM corpus_evidencecandidate WHERE id=:Id",{"Id": cid})
	
	while True:
		row = c.fetchone()
		if row == None:
			break
		
		c2 = conn.cursor()
		uId = row[0]
		c2.execute("SELECT alias FROM corpus_entityoccurrence WHERE id=:Id",{"Id": uId})
		leftrow = c2.fetchone()
		crop1 = leftrow[0]#.encode("utf-8");
		
		uId = row[1]
		c2.execute("SELECT alias FROM corpus_entityoccurrence WHERE id=:Id",{"Id": uId})
		rightrow = c2.fetchone()
		crop2 = rightrow[0]#.encode("utf-8");
		
		c3 = conn.cursor()
		uId = row[2]
		c3.execute("SELECT document_id,offset,offset_end FROM corpus_textsegment WHERE id=:Id",{"Id": uId})
		textrow = c3.fetchone()

		c4 = conn.cursor()
		uId = textrow[0]
		c4.execute("SELECT tokens FROM corpus_iedocument WHERE id=:Id",{"Id": uId})
		text = c4.fetchone()
		text = text[0].split(',')
		strategy =  text[textrow[1]:(textrow[2]+1)]
		#print(crop1,crop2,strategy)
		if mrow[2] == 1 or mrow[2] == 2 or mrow[2] == 3:
			crop_companion[crop1][crop2].append(strategy)
			crop_companion[crop2][crop1].append(strategy)
		else:
			crop_strategy[crop1].append(strategy)
c = conn.cursor()
c.execute("SELECT entityoccurrence_id,textsegment_id FROM corpus_entityoccurrence_segments")
while True:
	row = c.fetchone()
	if row == None:
		break

	c2 = conn.cursor()
	uId = row[0]
	c2.execute("SELECT alias,entity_id FROM corpus_entityoccurrence WHERE id=:Id",{"Id": uId})
	rightrow = c2.fetchone()
	crop = rightrow[0]#.encode("utf-8");
	entityid = rightrow[1]

	c5 = conn.cursor()
	c5.execute("SELECT kind_id FROM corpus_entity WHERE id=:Id",{"Id": entityid})
	entitykindrow = c5.fetchone()
	entitykind = entitykindrow[0]

	if entitykind == '1':
		c3 = conn.cursor()
		uId = row[1]
		c3.execute("SELECT document_id,offset,offset_end FROM corpus_textsegment WHERE id=:Id",{"Id": uId})
		textrow = c3.fetchone()

		c4 = conn.cursor()
		uId = textrow[0]
		c4.execute("SELECT tokens FROM corpus_iedocument WHERE id=:Id",{"Id": uId})
		text = c4.fetchone()
		text = text[0].split(',')
		strategy = text[textrow[1]:(textrow[2]+1)]

		crop_strategy[crop].append(strategy)

conn.close()

with open('crop_companion.pickle', 'wb') as handle:
	pickle.dump(crop_companion, handle)

with open('crop_strategy.pickle', 'wb') as handle:
	pickle.dump(crop_strategy, handle)

print "Crop companion"
print " "

for key,value in crop_companion.items():
	print(key)

print " "
print "Crop strategy"
print " "

for key, value in crop_strategy.items():
	print(key)
	#print(value)



