__author__ = 'SONY'

import pickle

#TODO: CHECK FOR MIXED CROPPINGS IN EXCEL FILE.
#TODO: HANDLE EXCEPTIONS

def recommend_fertilizer(crop, region=None, n=-1, p=-1, k=-1):
    #crop name pre process
    with open('fertpest/cropsyn.pickle', 'rb') as ch:
        crop_syn_list = pickle.load(ch)
    for scrop in crop_syn_list:
        for syn in scrop:
            if(crop==syn):
                crop = scrop[0]
                break;
    #get n,p,k value
    if n is -1 and p is -1 and k is -1 and region is not None:
        with open('fertpest/region_dict.pickle', 'rb') as handle:
            region_data = pickle.load(handle)
        try:
            area = float(region_data[region][5])
            #converting tons to kgs/ha
            n = float(region_data[region][2])/area * 907.185
            p = float(region_data[region][3])/area * 907.185
            k = float(region_data[region][4])/area * 907.185
        except KeyError,e:
            print "Region not in dataset."

    with open('fertpest/cropdata.pickle', 'rb') as handle:
        crop_data = pickle.load(handle)
        try:
            sugg_list = crop_data[crop]
            i=1
            output=""
            for suggestion in sugg_list:
                nsug = float(suggestion[0])-n if float(suggestion[0])-n>0 else 0
                psug = float(suggestion[1])-p if float(suggestion[1])-p>0 else 0
                ksug = float(suggestion[2])-k if float(suggestion[2])-k>0 else 0
                extrasug = suggestion[3]
                yieldsug = suggestion[4]
                output += "Suggestion"+str(i)+": "+\
                        str(round(nsug,2))+"kg/ha of Nitrate; " +\
                        str(round(psug,2))+"kg/ha of P2O5; " +\
                        str(round(ksug,2))+"kg/ha of K2O; "
                if(extrasug!=''):
                    output +=  "for "+extrasug
                if(yieldsug!=''):
                    output += " for a yield of " + yieldsug
                output+="\n"
                i+=1
        except KeyError,e:
            output = "No fertilizer recommendation for given crop."
        return output


# recommend_fertilizer("Rice", n=50, p=40, k=113)