Dependencies:
pyfuzzy tool kit http://pyfuzzy.sourceforge.net/
flask http://flask.pocoo.org/

Steps to run:
run python app.py
go to http://localhost:5000

Block diagram to code:
weather prediction: predictWeather.py
cost prediction: costLinearRegression (default) or Cost ANN (optional)
processed soil data: region_dict.pickle
Extracted farming strategy data using IEPY toolkit: strategy.sqlite
Statistical cleaning: StatisticalCleaning/
Fuzzy rule generation: FUZZY_RULE_GENERATION (also consists of agricultural NER module along with a Readme for the NER usage)
Fuzzy logic: fuzzyrecommendation.py
Fertilizer pesticide selection: fertpest/

Evaluation of results for 576 districts
run python app.py
go to http://localhost:5000/evaluation
