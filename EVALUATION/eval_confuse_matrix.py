import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
from listOfDistricts import *
from fuzzyrecommendation import *
from sklearn.metrics import *
import warnings
warnings.filterwarnings("ignore")
f = open('result_new.txt', 'w+')
def evaluate(cost = 0.3,main = False,region = None):
	y_true = list()
	y_pred = list()
	crops = list()
	if main:
		crops = ['arecanut','arhar','bajra','banana','barley','castor seeds','chilli','coconut','cotton','green gram','groundnut','horse gram','jowar','jute','maize','mustard','onion','ragi','rice','sesame','soyabean','sugarcane','sunflower','tobacco','turmeric','urad','wheat']
	else:
		crops = ['arecanut','arhar','bajra','banana','barley','castor seeds','chilli','coconut','cotton','green gram','groundnut','horse gram','jowar','jute','maize','mustard','onion','ragi','rice','sesame','soyabean','sugarcane','sunflower','tobacco','turmeric','urad','wheat','beetroot','black pepper','bottle gourd','brinjal','cabbage','cardamom','carrot','cashewnut','cauliflower','coriander','cucumber','drumstick','jackfruit','orange','potato','sweet potato','tapioca','tea','tomato']
	districts = listd
	if region:
		districts = list()
		districts.append(region)
	
	for dist in districts:
		visited_crops = list()
		try:
			result,weather = crop_recommendation(dist,2016,-1,-1,-1,cost,main)
		except IOError,e:
			continue
		except KeyError,e:
			print "crop_recommendation Exception", e
			continue
		try:
			historic_result = getDistrictCropRatio(dist,crops)
		except KeyError,e:
			print "historic_result Exception", e
			continue
		print "district",dist
		try:
			historic_result = [element.lower() for element in historic_result]
			for i in result:
				crop = result[i]["Name"]
				if crop in historic_result:
					y_true.append(crop)
					y_pred.append(crop)
					visited_crops.append(crop)
					historic_result.remove(crop)
			for i in result:
				crop = result[i]["Name"]
				if crop not in visited_crops:
					if historic_result:
						y_true.append(historic_result[0])
						y_pred.append(crop)
						historic_result.remove(historic_result[0])
					else:
						y_true.append('None')
						y_pred.append(crop)
		except ZeroDivisionError, e:
			continue
	crops.append('None')
	#print y_true
	#print y_pred
	matrix = confusion_matrix(y_true, y_pred, labels = crops)
	acc_score = accuracy_score(y_true, y_pred, normalize=False)
	f.write("Accuracy score: ")
	f.write(str(acc_score))
	f.write('\n')
	y_true = [crops.index(y) for y in y_true]
	y_pred = [crops.index(x) for x in y_pred]
	f.write("Individual crop report")
	f.write('\n')
	c = classification_report(y_true, y_pred, target_names=crops)
	f.write(c)
	f.write('\n')
	f.write("Precision: ")
	p = precision_score(y_true, y_pred)
	f.write(str(p))
	f.write('\n')
	f.write("Recall: ")
	r = recall_score(y_true, y_pred)
	f.write(str(r))
	f.write('\n')
	f.write("F1 score: ")
	f1 = f1_score(y_true, y_pred)
	f.write(str(f1))
	f.write('\n')
	f.write("confusion matrix: ")
	f.write('\n')
	f.write('         ')

	confusion = list()
	header = list()
	header.append("CROP")
	for x in crops:
		header.append(x)
		f.write(x)
		f.write('  ')
	i = 0
	f.write('\n')
	confusion.append(header)
	for x in matrix:
		row = list()
		f.write(crops[i])
		row.append(crops[i])
		f.write('  ')
		i = i+1
		for y in x:
			f.write(str(y))
			row.append(y)
			f.write('         ')
		confusion.append(row)
		f.write("\n")
	#print confusion
	eval_metrics = dict()
	eval_metrics["c"] = c
	eval_metrics["acc_score"] = acc_score
	eval_metrics["p"] = p
	eval_metrics["r"] = r
	eval_metrics["f1"] = f1
	if main:
		with open('eval_metricsm2.pickle', 'wb') as handle:
			pickle.dump(eval_metrics, handle)
		with open('confusionm2.pickle', 'wb') as handle:
			pickle.dump(confusion, handle)
	elif cost == 0.5:
		with open('eval_metrics52.pickle', 'wb') as handle:
			pickle.dump(eval_metrics, handle)
		with open('confusion52.pickle', 'wb') as handle:
			pickle.dump(confusion, handle)
	else:
		with open('eval_metrics32.pickle', 'wb') as handle:
			pickle.dump(eval_metrics, handle)
		with open('confusion32.pickle', 'wb') as handle:
			pickle.dump(confusion, handle)
	#return c,acc_score,p,r,f1,confusion
def evaluate_read(cost_ratio = 0.3, main = False):
	if main:
		with open('eval_metricsm2.pickle', 'rb') as handle:
			eval_metrics = pickle.load(handle)
		with open('confusionm2.pickle', 'rb') as handle:
			confusion = pickle.load(handle)
		return eval_metrics,confusion
	if cost_ratio == 0.5:
		with open('eval_metrics52.pickle', 'rb') as handle:
			eval_metrics = pickle.load(handle)
		with open('confusion52.pickle', 'rb') as handle:
			confusion = pickle.load(handle)
		return eval_metrics,confusion
	else:
		with open('eval_metrics32.pickle', 'rb') as handle:
			eval_metrics = pickle.load(handle)
		with open('confusion32.pickle', 'rb') as handle:
			confusion = pickle.load(handle)
		return eval_metrics,confusion
#evaluate(0.3,False,None)
#evaluate(0.5,False,None)
#evaluate(0.3,True,None)
