import csv, pickle, sys
from operator import itemgetter

eval_dict = dict()
district_crop_avg = dict()

with open('EVALUATION/apy_new.csv') as csvfile:
	
	reader = csv.DictReader(csvfile)

	for row in reader:
		
		district = row['District']
		crop = row['Crop']
		year = row['Year']

		row['Area'] = row['Area'].replace(",", "")
		row['Production'] = row['Production'].replace(",", "")

		if row['Area'] == '':
			row['Area'] = 0
		
		if row['Production'] == '':
			row['Production'] = 0

		row['Area'] = float(row['Area'])
		row['Production'] = float(row['Production'])

		if district not in eval_dict:
			eval_dict[district] = dict()
		
		if crop not in eval_dict[district]:
			eval_dict[district][crop] = dict()
			eval_dict[district][crop][year] = row

		elif crop in eval_dict[district] and year in eval_dict[district][crop]:
			eval_dict[district][crop][year]['Area'] = eval_dict[district][crop][year]['Area'] + row['Area']
			eval_dict[district][crop][year]['Production'] = eval_dict[district][crop][year]['Production'] + row['Production']
		else:
			eval_dict[district][crop][year] = row


	crops = ['arecanut','arhar','bajra','banana','barley','castor seeds','chilli','coconut','cotton','green gram','groundnut','horse gram','jowar','jute','maize','mustard','onion','ragi','rice','sesame','soyabean','sugarcane','sunflower','tobacco','turmeric','urad','wheat','beetroot','black pepper','bottle gourd','brinjal','cabbage','cardamom','carrot','cashewnut','cauliflower','coriander','cucumber','drumstick','jackfruit','orange','potato','sweet potato','tapioca','tea','tomato']

	for district in eval_dict.keys():
		district_crop_avg[district] = dict()
		for c in crops:
			area = 0
			production = 0
			crop = c.capitalize()
			#print district,crop
			district_crop_avg[district][crop] = dict()

			if district not in eval_dict or crop not in eval_dict[district]:
				area = 0
				production = 0
				#print "A"
			else:
				#print "B"
				count = 0
				area = 0
				production = 0

				for year in eval_dict[district][crop].keys():
					if eval_dict[district][crop][year]['Area'] != 0.0:
						area += eval_dict[district][crop][year]['Area']
						production += eval_dict[district][crop][year]['Production']			
						count = count + 1

				if count == 0:
					count = 1
				area = area / count
				production = production / count

			district_crop_avg[district][crop]['Area'] = area
			district_crop_avg[district][crop]['Production'] = production


with open('EVALUATION/historicDataReader.pickle', 'wb') as handle:
    pickle.dump(district_crop_avg, handle)


district_crop_avg1 = pickle.load( open('EVALUATION/historicDataReader.pickle', "rb" ) )



def getStatistics(district, crop):
	if district not in district_crop_avg1 or crop not in district_crop_avg1[district]:
		return district_crop_avg1[district][crop]
	else:
		result = dict()
		result['Area'] = 0
		result['Production'] = 0
		return result


def getDistrictCropRatio(district,crops = None):
	if crops == None:
		crops = ['arecanut','arhar','bajra','banana','barley','castor seeds','chilli','coconut','cotton','green gram','groundnut','horse gram','jowar','jute','maize','mustard','onion','ragi','rice','sesame','soyabean','sugarcane','sunflower','tobacco','turmeric','urad','wheat','beetroot','black pepper','bottle gourd','brinjal','cabbage','cardamom','carrot','cashewnut','cauliflower','coriander','cucumber','drumstick','jackfruit','orange','potato','sweet potato','tapioca','tea','tomato']
	crop_ratio = dict()
	for crop in crops:
		c = crop.capitalize()
		#crop_ratio[c] = float(district_crop_avg1[district][c]['Production'] / district_crop_avg1[district][c]['Area'])
		area = district_crop_avg1[district][c]['Area']
		#print c, area
		if area > 0:
			crop_ratio[c] = district_crop_avg1[district][c]['Area']
	
	return sorted(crop_ratio.keys(), key=crop_ratio.get, reverse=True)


#print getStatistics(sys.argv[1], sys.argv[2])
#print getDistrictCropRatio(sys.argv[1])
