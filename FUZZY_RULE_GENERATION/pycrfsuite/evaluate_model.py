from collections import Counter
from itertools import chain
import nltk
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.preprocessing import LabelBinarizer
import sklearn
import pycrfsuite

tagger = pycrfsuite.Tagger()
tagger.open('datainput.features.crfsuite')

# info = tagger.info()

def print_transitions(trans_features):
    for (label_from, label_to), weight in trans_features:
        print("%-6s -> %-7s %0.6f" % (label_from, label_to, weight))

# print("Top likely transitions:")
# print_transitions(Counter(info.transitions).most_common(15))

# print("\nTop unlikely transitions:")
# print_transitions(Counter(info.transitions).most_common()[-15:])



def print_state_features(state_features):
    for (attr, label), weight in state_features:
        print("%0.6f %-6s %s" % (weight, label, attr))    

# print("Top positive:")
# print_state_features(Counter(info.state_features).most_common(20))

# print("\nTop negative:")
# print_state_features(Counter(info.state_features).most_common()[-20:])


def crop_classification_report(y_true, y_pred):
    lb = LabelBinarizer()
    y_true_combined = lb.fit_transform(list(chain.from_iterable(y_true)))
    y_pred_combined = lb.transform(list(chain.from_iterable(y_pred)))
        
    tagset = set(lb.classes_) - {'O'}
    tagset = sorted(tagset, key=lambda tag: tag.split('-', 1)[::-1])
    class_indices = {cls: idx for idx, cls in enumerate(lb.classes_)}
    
    print len(y_true_combined)
    print len(y_pred_combined)

    return classification_report(
        y_true_combined,
        y_pred_combined,
        labels = [class_indices[cls] for cls in tagset],
        target_names = tagset,
    )				
# y_pred = [tagger.tag(xseq) for xseq in X_test]
# print(crop_classification_report(y_test, y_pred))
