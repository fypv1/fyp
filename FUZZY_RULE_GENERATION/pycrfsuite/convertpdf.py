import os
from pdf_read import  convertPDF

def list_files(file):
    print "processing" + str(file)
    text = convertPDF(file)
    filename = "crop_data.txt"
    with open(filename, "w") as f:
        f.write(text)

list_files('crop_data.pdf')