import pycrfsuite, re, csv
from evaluate_model import crop_classification_report
##########################################   pre-process #####################################
def pre_process(filename):
	with open(filename, "r") as f:
        # splitting by dot.
	    sents = re.split("[O]+\t\d+\t\d+\t[.]\t[.]\t[.]\t[A-Z]+\t[A-Z]+\t[A-Z]+\t[A-Z]+\t[A-Z\-]+\t[A-Z\-]+\t[A-Z\-]+\t[A-Z\-]+\t[A-Z\-]+\t[A-Z\-]+\t[A-Z\-]+", f.read())
	train_sents = list()
	for i in range(len(sents)):
		train_sents.append([])
		words_features = sents[i].split("\n")

		for wf in words_features:
			train_sents[i].append(tuple(filter(None,wf.split("\t")))) #to remove empty string ''
		train_sents[i] = filter(None, train_sents[i])	#to remove empty tuple ()
	train_sents = filter(None, train_sents)	#to remove empty list []
	return train_sents


# print train_sents[len(train_sents)-1]
# print "********pre-processing testing input ************"
# test_sents = pre_process("sample data/data")
# print test_sents[len(test_sents)-1]

########################################################## pre-process end ###############################

####################################################### feature extraction ###############################

def word2features(sent, i):
    # sent[i] is a word
    word = sent[i][3]
    postag = sent[i][5]
    dic_features = sent[i][7:17]
    k=0
    dicf = list()
    for df in dic_features:
        dicf.append('dicf'+str(k)+'=' + str(df))
        k+=1
        
    features = [
        'bias',
        'word.lower=' + word.lower(),
        'word[-3:]=' + word[-3:],
        'word[-2:]=' + word[-2:],
        'word.isupper=%s' % word.isupper(),
        'word.istitle=%s' % word.istitle(),
        'word.isdigit=%s' % word.isdigit(),
        'postag=' + postag,
        'postag[:2]=' + postag[:2],
    ]
    features.extend(dicf)
    if i > 0:
        word1 = sent[i-1][3]
        postag1 = sent[i-1][5]
        features.extend([
            '-1:word.lower=' + word1.lower(),
            '-1:word.istitle=%s' % word1.istitle(),
            '-1:word.isupper=%s' % word1.isupper(),
            '-1:postag=' + postag1,
            '-1:postag[:2]=' + postag1[:2],
        ])
    else:
        features.append('BOS')
        
    if i < len(sent)-1:
        word1 = sent[i+1][3]
        postag1 = sent[i+1][5]
        features.extend([
            '+1:word.lower=' + word1.lower(),
            '+1:word.istitle=%s' % word1.istitle(),
            '+1:word.isupper=%s' % word1.isupper(),
            '+1:postag=' + postag1,
            '+1:postag[:2]=' + postag1[:2],
        ])
    else:
        features.append('EOS')
                
    return features


def sent2features(sent):
    return [word2features(sent, i) for i in range(len(sent))]

def sent2labels(sent):
    return [word[0] for word in sent]
    # return [label for token, postag, label in sent]

def sent2tokens(sent):
    return [word[3] for word in sent]    

# print sent2features(train_sents[0])[0]

################################################ feature extraction end ##################################

################################################ trainer #################################################
def train(train_sents):
	X_train = [sent2features(s) for s in train_sents]
	y_train = [sent2labels(s) for s in train_sents]

	trainer = pycrfsuite.Trainer(verbose=False)

	for xseq, yseq in zip(X_train, y_train):
	    trainer.append(xseq, yseq)

	trainer.set_params({
	    'c1': 0,   # coefficient for L1 penalty
	    'c2': 1,  # coefficient for L2 penalty
	    'max_iterations': 50,  # stop earlier

	    # include transitions that are possible, but not observed
	    'feature.possible_transitions': True
	})

	trainer.train('datainput.features.crfsuite')

print "************************* training ************************"
train_sents = list()
for i in range(2,5):
    filename = "train data/data"+str(i)
    train_sents.extend(pre_process(filename))
train(train_sents)

#################################################### training over #########################################

################################################### tagging ###############################################


tagger = pycrfsuite.Tagger()
tagger.open('datainput.features.crfsuite')
y_pred = list()
y_test = list()

print "********pre-processing training input ************"
for i in range(1,7):
    tokens = list()
    tags = list()
    actual_tag = list()
    filename = "train data/data"+str(i)
    test_sents = pre_process(filename)

    for example_sent in test_sents:
    	tokens.extend(sent2tokens(example_sent))
    	tags.extend(tagger.tag(sent2features(example_sent)))
    	actual_tag.extend(sent2labels(example_sent))

    # write to file
    # train_sents has list of list
    features = [sent for sents in test_sents for sent in sents]
    filename = "ner/data"+str(i)+"_ner"
    with open(filename, 'w') as f:
        for tag, feature in zip(tags, features):
            f.write(tag + '\t' + '\t'.join(feature) + '\n')

    for example_sent in test_sents:
        print ' '.join(sent2tokens(example_sent))

        print "Predicted:", ' '.join(tagger.tag(sent2features(example_sent)))
        print "Correct:  ", ' '.join(sent2labels(example_sent)) 
        print "-----------------------------------------------------------------"


############################## evaluation #############################################

    x_test = [sent2features(s) for s in test_sents]
    y_test.extend([sent2labels(s) for s in test_sents])
    y_pred.extend([tagger.tag(xseq) for xseq in x_test])
print(crop_classification_report(y_test, y_pred))

################################################### tagging over ########################################
