for n in range(1,7):
	sfilename = "stanford/data"+str(n)+"_s"
	with open(sfilename, "r") as f:
		stanfordlines = f.read()

	filename = "ner/data"+str(n)+"_ner"
	with open(filename, "r") as f:
		lines = f.readlines()
	print len(lines)

	outputlines = list()
	for line in lines:
		line = line.split('\t')
		index = stanfordlines.find(line[4]+"/")
		if(index!=-1):
			i=index+len(line[4])+1
			while(i<len(stanfordlines) and stanfordlines[i]!='\n'):
				i+=1
			line.insert(0, stanfordlines[index+len(line[4])+1:i])
		else:
			line.insert(0, 'O')
		line = '\t'.join(line)
		outputlines.append(line)

	ofilename = "sner/data"+str(n)+"_sner"
	with open(ofilename, "w+") as f:
		f.writelines(outputlines)