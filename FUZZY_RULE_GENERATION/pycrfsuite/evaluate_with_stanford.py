lines = list()
for i in range(1,7):
	filename = "sner/data"+str(i)+"_sner"
	with open(filename, 'r') as f:
		lines += f.readlines()

# 0 - stanford output
# 1 - ner output
# 2 - correct output
print len(lines)
stanfordtags=0
nertags=0
newtags=0
editedtags=0
nerfp=0.0
nerfn=0.0
nertp=0.0
nertn=0.0
stanfordfp=0.0
stanfordfn=0.0
stanfordtp=0.0
stanfordtn=0.0

# copy stanford tags to correct tags if correct tag is O
for line in lines:
	line = line.split('\t')

	if(line[0]!='O' and line[2]=='O'):
		line[2]=line[0]
	if(line[0]!='O' and line[1]=='O'):
		line[1]=line[0]

	if(line[0]!='O'):
		stanfordtags+=1
	if(line[1]!='O'):
		nertags+=1

# check if stanford tag is O but ner tag is not O (new tags)
	# check if ner tag is equal to correct tag 
	# check if ner tag is not equal to correct tag
	
	if(line[0]=='O' and line[1]!='O'):
		newtags+=1
		# result is positive
		if(line[1]==line[2]):
			nertp+=1
		else:
			nerfp+=1		

# check if stanford tag is not O and ner tag is not O (changed tags)
	# check if ner tag is equal to correct tag 
	# check if ner tag is not equal to correct tag

	if(line[0]!='O' and line[1]!='O'):
		if(line[0]!=line[1]):
			editedtags+=1
		# result is positive
		if(line[1]==line[2]):
			nertp+=1
		else:
			nerfp+=1
	
	if(line[1]=='O'):
		# result is negative
		if(line[1]==line[2]):
			nertn+=1
		else:
			nerfn+=1

# check if stanford tag == correct tag
# check if stanford tag != correct tag
	if(line[0]!='O'):
		#result is positive
		if(line[0]==line[2]):
			stanfordtp +=1
		else:
			stanfordfp +=1

	if(line[0]=='O'):
		#result is negative
		if(line[0]==line[2]):
			stanfordtn +=1
		else:
			stanfordfn +=1

stansen = stanfordtp/(stanfordtp+stanfordfn)
nersen = nertp/(nertp+nerfn)
stansp = stanfordtn/(stanfordtn+stanfordfp)
nersp = nertn/(nertn+nerfp)
stanppv = stanfordtp/(stanfordtp+stanfordfp)
nerppv = nertp/(nertp+nerfp)
stannpv = stanfordtn/(stanfordtn+stanfordfn)
nernpv = nertn/(nertn+nerfn)

print "Number of entities tagged in existing ner "+str(stanfordtags)
print "Number of entities tagged in our ner "+str(nertags)
print "Number of new tags "+str(newtags)
print "Number of modified tags "+str(editedtags)
print "**********************************Existing system***********************************"
# print "True positive "+str(stanfordtp)
# print "False positive "+str(stanfordfp)
# print "True negative "+str(stanfordtn)
# print "False negative "+str(stanfordfn)
print "Sensitivity "+str(stansen)
print "Specificity "+str(stansp)
print "Precision "+str(stanppv)
print "NPV "+str(stannpv)

print "**********************************Our system***********************************"
# print "True positive "+str(nertp)
# print "False positive "+str(nerfp)
# print "True negative "+str(nertn)
# print "False negative "+str(nerfn)
print "Sensitivity "+str(nersen)
print "Specificity "+str(nersp)
print "Precision "+str(nerppv)
print "NPV "+str(nernpv)


