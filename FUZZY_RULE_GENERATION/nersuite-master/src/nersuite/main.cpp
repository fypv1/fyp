#ifdef    HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include "nersuite.h"

#define DEFAULT_MODEL_FILE	"model.m"

using namespace std;


int main(int argc, char* argv[])
{
	if (argc < 4) {
		cerr << "argument error";
		return 1;
	}

	string  ner_dir = argv[0];
	ner_dir = ner_dir.substr(0, ner_dir.find_last_of("/"));

	string	mode = argv[1];

	NER::Suite nersuite(argc-2, &argv[2]);

	// Train or Tag
	if (mode == MODE_LEARN) {
		nersuite.learn();
	} else if (mode == MODE_TAG) {            // Tag each sentence
		nersuite.tag();
	} else {
		cerr << " The first argument must be either \"" << MODE_LEARN << "\" or \"" << MODE_TAG << "\"" << endl;
		return -1;
	}

	return 0;
}

