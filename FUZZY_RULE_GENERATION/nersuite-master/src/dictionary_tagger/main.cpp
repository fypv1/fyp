#include <list>
#include <iostream>
#include <exception>
#include <algorithm>
#include "../nersuite_common/dictionary.h"
#include "../nersuite_common/option_parser.h"
#include "../nersuite_common/text_loader.h"
#include "../nersuite_common/string_utils.h"
#include "../nersuite_common/nersuite_exception.h"
#include "sentence_tagger.h"

using namespace std;

void print(vector<string> &item);


int main(int argc, char* argv[])
{
	NER::OPTION_PARSER opt_parser;
	opt_parser.parse(argc, argv);
	const vector<const char*>& args = opt_parser.get_args();
	if (args.size() < 2)
	{
		exit(1);
	}
	int		normalize_type = NER::NormalizationUnknown;
	string	normalize_option;
	if (opt_parser.get_value("-n", normalize_option))
	{
		normalize_type = NER::NormalizeNone;

		if (normalize_option == "none")
		{
			// do nothing
		}
		else
		{
			if (normalize_option.find('c') != string::npos)
			{
				normalize_type |= NER::NormalizeCase;
			}
			if (normalize_option.find('n') != string::npos)
			{
				normalize_type |= NER::NormalizeNumber;
			}
			if (normalize_option.find('s') != string::npos)
			{
				normalize_type |= NER::NormalizeSymbol;
			}
			if (normalize_option.find('t') != string::npos)
			{
				normalize_type |= NER::NormalizeToken;
			}
		}
	}

	string	multidoc_separator = "";
	bool	multidoc_mode = opt_parser.get_value("-multidoc", multidoc_separator);

	try
	{
		NER::Dictionary dict(args[1]);
		dict.open();

		// If no normalization specified, set according to DB
		if ( normalize_type == NER::NormalizationUnknown )
		{
			normalize_type = dict.get_normalization_type();
		}

		// Check for mismatch in normalization type
		if ( dict.get_normalization_type() != NER::NormalizationUnknown &&
		     dict.get_normalization_type() != normalize_type)
		{
			std::cerr << "Warning: given normalization (" << normalize_type << ") does not match DB normalization (" << dict.get_normalization_type() << "). Tagging performance may be decreased.\n";
		}
		
		NER::SentenceTagger::set_normalize_type(normalize_type);

		// Tag input with a dictionary
		NER::SentenceTagger	one_sent;
		
		while (! cin.eof()) 
		{
			// 1. Skip if it is a blank line
			if( one_sent.read(cin, multidoc_separator) == 0 ) {
				continue;
			}
			
			// 2. Print comment lines
			if( multidoc_mode && one_sent.get_content_type() == 1 ) {
				for( V2_STR::iterator irow = one_sent.begin(); irow != one_sent.end(); ++ irow) {
					cout << irow->front() << endl;     // Comment is stored as a string at [0] position
				}
				cout << endl;
				
				continue;
			}
		  
			// 3. Tag a sentence
			one_sent.tag_nes(dict);     // Find the best NE candidate at the beginning of each word in a sentence

			// 4. Print the output
			if (!one_sent.empty())
			{
				for_each(one_sent.begin(), one_sent.end(), print);
				cout << endl;
			}
		}
	}
	catch (const cdbpp::cdbpp_exception& e)
	{
		// Abort if something went wrong...
		std::cerr << "ERROR: " << e.what() << std::endl;
		return 1;
	}
	catch (const NER::nersuite_exception& ex)
	{
		cerr << ex.what();
		return 1;
	}
	return 0;
}

void print(vector<string> &item)
{
	for(vector<string>::iterator itr1 = item.begin(); itr1 != item.end(); itr1++) {
		cout << *itr1;
		if((itr1 + 1) == item.end()) {
			cout << endl;
		}else {
			cout << "\t";
		}
	}
}
