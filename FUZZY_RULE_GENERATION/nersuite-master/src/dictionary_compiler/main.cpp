
#include <iostream>
#include <exception>
#include <cstdlib>
#include "../nersuite_common/dictionary.h"
#include "../nersuite_common/option_parser.h"

using namespace std;


int main(int argc, char *argv[])
{
	NER::OPTION_PARSER opt_parser;
	opt_parser.parse(argc, argv);
	const vector<const char*>& args = opt_parser.get_args();
	if (args.size() != 3)
	{
		exit(1);
	}
	int	normalize_type = NER::NormalizeNone;
	string normalize_option;
	if (opt_parser.get_value("-n", normalize_option))
	{
		if (normalize_option == "none")
		{
			// do nothing
		}
		else
		{
			if (normalize_option.find('c') != string::npos)
			{
				normalize_type |= NER::NormalizeCase;
			}
			if (normalize_option.find('n') != string::npos)
			{
				normalize_type |= NER::NormalizeNumber;
			}
			if (normalize_option.find('s') != string::npos)
			{
				normalize_type |= NER::NormalizeSymbol;
			}
			if (normalize_option.find('t') != string::npos)
			{
				normalize_type |= NER::NormalizeToken;
			}
		}
	}

	try
	{
		NER::Dictionary dc(args[2]);
		dc.build(args[1], normalize_type);
	}
	catch (const exception& ex)
	{
		cerr << ex.what();
	}

	return 0;
}
