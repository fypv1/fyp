Agricultural NER

NOTE: Installation of below tools requires Python 2.7+ or 3.3+

********* Preprocessing ***************************
Tool: NERSuite

1. Tokenisation
2. POS tagging
3. Dictionary compiler
are done using this tool.

Installation Guide for NERSuite:
http://nersuite.nlplab.org/installation_guide.html

NOTE: NERSuite requires Unix environment.

Usage:
Tokenisation and POS tagging:
nersuite_tokenizer < input.txt | nersuite_gtagger -d ~/models/gtagger/ > output.ner 

Dictionary compilation:
nersuite_dic_compiler -n cnt dictionary.txt dictionary.cdbpp

Other NERSuite commands can be found at:
http://nersuite.nlplab.org/command_reference.html

********* CRFSuite for python *********************
Training and tagging is done using PyCRFsuite which 
is a python extension of CRFsuite

Installation:
pip install python-crfsuite

********* NER evaluation *******************************
Tool: sklearn
Installation:
pip install sklearn-crfsuite

StanfordNER: 
Stanford NER is used for evaluation purpose to
compare with proposed NER.

Installation Guide:
Download StanfordNER from 
http://nlp.stanford.edu/software/CRF-NER.shtml#Download

Usage for StanfordNER:
java -mx1g edu.stanford.nlp.ie.NERClassifierCombiner -ner.model classifiers/english.all.3class.distsim.crf.ser.gz,classifiers/english.conll.4class.distsim.crf.ser.gz,classifiers/english.muc.7class.distsim.crf.ser.gz -outputFormat slashTags -textFile input.txt > output.tsv 

NOTE: It requires java v1.8+
************ Agricultural NER **************************
Usage:
From prfsuite folder run
python ner.py

NOTE: it requires pycrfsuite and sklearn to be installed

appendoutput.py - to append StanfordNER and Agricultural 
NER output
evaluatemodel.py - to find F-Score of the model
evaluate_with_stanford.py - to evaluate comparing with 
Stanford NER.

************** Relation Extraction *********************
NOTE: It requires java v1.8+

Download Stanford CoreNLP 
http://stanfordnlp.github.io/CoreNLP/

Usage:
Training:
java edu.stanford.nlp.ie.machinereading.MachineReading --arguments domains/roth/roth.properties

Tagging:
java -cp "-CLASSPATH-":. StanfordCoreNlpDemo input.txt input.txt.out
