import pickle, collections, sys
from getTitle import getTitle

def dd():
	return collections.defaultdict(list)

crop_companion = collections.defaultdict(dd)

def getCropStrategy(crop):
	strategy = collections.defaultdict(list)
	with open('StatisticalCleaning/synonym_crops_strategy.pickle', 'rb') as handle:
		strategy = pickle.load(handle)
		crop = getTitle(crop)
		if crop in strategy:
			return strategy[crop]
		else:
			return list()

def getCropRank(crop):
	rank = dict()
	with open('StatisticalCleaning/synonym_crops_rank.pickle', 'rb') as handle:
		rank = pickle.load(handle)
		crop = getTitle(crop)
		if crop in rank:
			return rank[crop]
		else:
			return 0.25

def getMixedCropStrategy(crop):
	strategy = dict()
	with open('StatisticalCleaning/synonym_mixed_crops_strategy.pickle', 'rb') as handle:
		strategy = pickle.load(handle)
		crop = getTitle(crop)
		if crop in strategy:
			return strategy[crop]
		else:
			return dict()

def getMixedCropStrategies(crop):
	with open('StatisticalCleaning/synonym_mixed_crops_strategy_mixed.pickle', 'rb') as handle:
		crop_companion = pickle.load(handle)
		crop = getTitle(crop)
		if crop in crop_companion:
			return crop_companion[crop]
		else:
			return dict()

def getMixedCropRank(crop):
	rank = dict()
	with open('StatisticalCleaning/synonym_mixed_crops_rank.pickle', 'rb') as handle:
		rank = pickle.load(handle)
		crop = getTitle(crop)
		if crop in rank:
			return rank[crop]
		else:
			return 0.25


#crop = sys.argv[1]
#print getCropStrategy(crop)
