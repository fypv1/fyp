import sys,os,pickle,operator,codecs
import antlr3,fuzzy
import fuzzy.storage.fcl.Reader
from predictWeather import predict_weather
from costLinearRegression import getCost
sys.path.insert(0,'StatisticalCleaning/')
from StatisticalCleaning.getStrategy import *
from StatisticalCleaning.getTitle import *
sys.path.insert(0,'fertpest/')
from fertilizer_recommender import *
from pesticide import *
sys.path.insert(0,'EVALUATION/')
from historicDataReader import *


def crop_recommendation(region,year = 2016 ,n = -1,p = -1,k = -1,costratio = 0.5, main = False):
	region_dict = dict()
	with open('region_dict.pickle', 'rb') as handle:
		region_dict = pickle.load(handle)
	synonyms = { 'arecanut' : 'Arecanut' , 'arhar' : 'Arhar - Pigeon Pea - Toor Dal - Red Gram' , 'bajra' : 'Bajra - Pearl Millet' , 
	'banana' : 'Banana' , 'barley' : 'Barley' , 'castor seeds' : 'Castor seeds' , 'chilli' : 'Chilli' ,'coconut' : 'Coconut','cotton' : 'Cotton',
	'green gram' : 'Green Gram - Moong', 'groundnut' : 'Groundnut','horse gram' : 'Horse Gram','jowar' : 'Jowar - Sorghum','jute' : 'Jute',
	'maize' : 'Maize - Corn','mustard' : 'Mustard','onion' : 'Onion','green peas' : 'Green Peas - Pea','ragi' : 'Ragi - Finger Millet',
	'rice' : 'Rice','sesame' : 'Sesame','soyabean' : 'Soyabean - Soybean','sugarcane' : 'Sugarcane','sunflower' : 'Sunflower - Oilseed',
	'tobacco' : 'Tobacco','turmeric' : 'Turmeric','urad' : 'Urad - Black Gram','wheat' : 'Wheat',
	'beans' : 'Beans - French Beans','beetroot' : 'Beetroot - Beet','bhindi' : 'Bhindi - Ladies Finger - Okra','bitter gourd' : 'Bitter Gourd',
	'black pepper' : 'Black Pepper','bottle gourd' : 'Bottle Gourd','brinjal' : 'eggplant - Brinjal','cabbage' : 'Cabbage','cardamom' : 'Cardamom',
	'carrot' : 'Carrot','cashewnut' : 'Cashewnut - Cashew','cauliflower' : 'Cauliflower','coffee' :'Coffee','coriander' : 'Coriander',
	'cucumber' : 'Cucumber','drumstick' : 'Drumstick','jackfruit' : 'Jackfruit','mango' : 'Mango','orange' : 'Orange - Citrus',
	'potato' : 'Potato','sweet potato' : 'Sweet Potato','tapioca' : 'Cassava - Tapioca','tea' : 'Tea','tomato' : 'Tomato' }
	maincrops = ['','arecanut','arhar','bajra','banana','barley','castor seeds','chilli','coconut','cotton','green gram','groundnut','horse gram','jowar','jute','maize','mustard','onion','ragi','rice','sesame','soyabean','sugarcane','sunflower','tobacco','turmeric','urad','wheat']
	sidecrops = ['','beetroot','black pepper','bottle gourd','brinjal','cabbage','cardamom','carrot','cashewnut','cauliflower','coriander','cucumber','drumstick','jackfruit','orange','potato','sweet potato','tapioca','tea','tomato']
	# preallocate input and output values
	my_input = {
	        "temp" : 0.0,
	        "rain" : 0.0,
	        "ph" : 0.0
	        }
	my_output = {
	        "crop" : 0.0
	        }
	crops = dict()
	for month in range(0,6):
		for days in range(365,366):
			# set input values
			weatherDict = predict_weather(region, year)
			startmonth = 1 + month
			endmonth = 1 + month + days/30
			totalprecipitation = 0
			totalavgTemp = 0
			for i in range(startmonth,endmonth+1):
				totalprecipitation = totalprecipitation + weatherDict["precipitation"][i%12]
				totalavgTemp = totalavgTemp + weatherDict["avgTemp"][i%12]
			avgTemp = float(totalavgTemp)/float((days/30) + 1)
			my_input["temp"] = avgTemp
			my_input["rain"] = totalprecipitation
			if(region in region_dict.keys()):
				my_input["ph"] = region_dict[region][0]
			else:
				my_input["ph"] = 6.0
			# calculate
			system = fuzzy.storage.fcl.Reader.Reader().load_from_file("definitionsmain.fcl")
			system.calculate(my_input, my_output)
			#print my_output["crop"]
			# now use outputs
			crop = maincrops[int(my_output["crop"])]
			if(crop != ""):
				crops[crop] = startmonth
			if not main:
				system = fuzzy.storage.fcl.Reader.Reader().load_from_file("definitionsside.fcl")
				system.calculate(my_input, my_output)
				#print my_output["crop"]
				# now use outputs
				crop = sidecrops[int(my_output["crop"])]
				if(crop != ""):
					crops[crop] = startmonth
			
			system = fuzzy.storage.fcl.Reader.Reader().load_from_file("definitionsmain2.fcl")
			system.calculate(my_input, my_output)
			#print my_output["crop"]
			# now use outputs
			crop = maincrops[int(my_output["crop"])]
			if(crop != ""):
				crops[crop] = startmonth
			
			if not main:
				system = fuzzy.storage.fcl.Reader.Reader().load_from_file("definitionsside2.fcl")
				system.calculate(my_input, my_output)
				#print my_output["crop"]
				# now use outputs
				crop = sidecrops[int(my_output["crop"])]
				if(crop != ""):
					crops[crop] = startmonth
	cropratio = list()
	if main:
		cropratio = getDistrictCropRatio(region,maincrops)
	else:
		cropratio = getDistrictCropRatio(region)
	i = 0
	'''for x in cropratio:
		if x != "Rice" and x != "Sugarcane":
			crops[x.lower()] = 1
			#print x.lower()
			i = i + 1
			if (i == 2):
				break'''
	#print(crops)
	ranks = dict()
	for (crop1,month) in crops.items():
		rank = 0
		if costratio == 0.5:
			rank = (6-month)/6 * (0.5) + getCost(crop1.capitalize(), year) * (0.5)
		else:
			rank = (6-month)/6 * float(1/3) + getCropRank(crop1) * float(1/3) + getCost(crop1.capitalize(), year) * float(1/3)
		ranks[crop1] = rank
	result_crops = []  
	i = 0
	for key, value in sorted(ranks.items(), key=operator.itemgetter(1), reverse = True):  
		result_crops.append(key)
		i = i + 1
		if (costratio == 0.5 and i == 5):
			break
	#add farming strategies, mixed crops, fertilizer, pesticide for the list of crops and print
	result_strategy = dict()
	result_seedling = dict()
	result_spacing = dict()
	result_irrigation = dict()
	result_growthperiod = dict()
	result_harvesting = dict()
	result_miscstrategy = dict()
	result_mixed = dict()
	result_fert = dict()
	result_pest = dict()
	for crop in result_crops:
		result_strategy[crop] = list()
		result_strategy[crop] = getCropStrategy(crop)
		for i in range(len(result_strategy[crop])):
			if "." in result_strategy[crop][i]:
				dot = result_strategy[crop][i].index(".")
				#print dot,result_strategy[crop][i]
				result_strategy[crop][i] = result_strategy[crop][i][0 : (dot+1)]
		'''for i in range(len(result_strategy[crop])):
			if i < len(result_strategy[crop]) - 1 and result_strategy[crop][i] == ".":
				result_strategy[crop] = result_strategy[crop][0: i + 1]
		'''
		result_mixed[crop] = getMixedCropStrategy(crop)
		result_seedling[crop] = list()
		result_spacing[crop] = list()
		result_irrigation[crop] = list()
		result_growthperiod[crop] = list()
		result_harvesting[crop] = list()
		result_miscstrategy[crop] = list()
		for v in result_strategy[crop]:
			curv = " ".join(v)
			curv = ''.join([i if ord(i) < 128 else ' ' for i in curv])
			#print newv
			curv = curv.encode('ascii')
			curv = curv.decode('ascii','ignore')
			if("seedling" in curv):
				result_seedling[crop].append(curv)
			elif("treat" in curv and "seed" in curv):
				result_seedling[crop].append(curv)
			elif("spac" in curv or "distance" in curv or "cm" in curv):
				result_spacing[crop].append(curv)
			elif("irrigat" in curv or "water" in curv):
				result_irrigation[crop].append(curv)
			elif("year" in curv or "month" in curv or "period" in curv or "days" in curv):
				result_growthperiod[crop].append(curv)
			elif("harvest" in curv or "yield" in curv):
				result_harvesting[crop].append(curv)
			else:
				result_miscstrategy[crop].append(curv)
		#seedling treat (seeding)
		#irrigat water (irrigation)
		#spac distance cm (plant spacing)
		#year month period days (growth period)
		#harvest yield (harvesting)
		result_fert[crop] = recommend_fertilizer(crop.lower(),region,n,p,k)
		result_pest[crop] = pest_recommend(crop.lower())
	final_result = dict()
	i = 1
	weatherprint = predict_weather(region, year)
	for crop in result_crops:
		final_result[i] = dict()
		final_result[i]["Name"] = crop
		final_result[i]["Crop"] = list()
		final_result[i]["Crop"].append(synonyms[crop])
		final_result[i]["Fertilizer"] = result_fert[crop]
		final_result[i]["Cost"] = getCost(crop.capitalize(), year)
		final_result[i]["avgTemp"] = weatherprint["avgTemp"]
		final_result[i]["precipitation"] = weatherprint["precipitation"]
		final_result[i]["Pesticide"] = result_pest[crop]
		final_result[i]["Mixed cropping"] = result_mixed[crop]
		final_result[i]["Miscellaneous farming strategy"] = result_miscstrategy[crop]
		final_result[i]["Seed treatment"] = result_seedling[crop]
		final_result[i]["Spacing"] = result_spacing[crop]
		final_result[i]["Irrigation"] = result_irrigation[crop]
		final_result[i]["Growth period"] = result_growthperiod[crop]
		final_result[i]["Harvesting"] = result_harvesting[crop]
		i = i+1
	return final_result,weatherDict

'''
final_result = crop_recommendation(sys.argv[1])
for x,v in final_result.items():
	print x 
	for k,v2 in v.items():
		print k
'''

'''StatisticalCleaning demo
strategy =  getCropStrategy("Maize")
mixed = getMixedCropStrategy("Maize")
mixed2 = dict()
for key,value in mixed.items():
	ls = value
	mixed2[key] = list()
	for v in ls:
		newv = " ".join(v)
		mixed2[key].append(newv + '\n')
newv = ""
for v in strategy:
	curv = " ".join(v)
	curv = ''.join([i if ord(i) < 128 else ' ' for i in curv])
	newv = (newv + curv + '\n')
print "rank: ",getCropRank("Maize")
print " "
print " "
print "Mixed cropping: "
print mixed2
print " "
print " "
print "Farming strategies: "
print newv'''

'''res  = crop_recommendation(sys.argv[1])
for x in res:
	print x,'\n' '''