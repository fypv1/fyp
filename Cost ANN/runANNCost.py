from pybrain.datasets import SupervisedDataSet
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer

from pybrain.datasets            import ClassificationDataSet
from pybrain.utilities           import percentError
from pybrain.tools.shortcuts     import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules   import SoftmaxLayer

from pylab import ion, ioff, figure, draw, contourf, clf, show, hold, plot
from scipy import diag, arange, meshgrid, where
from numpy.random import multivariate_normal
import sys, pickle, csv


atData = list()
pData = list()
name = sys.argv[1]

####################################################################################
######################            Pickle file name            ######################
####################################################################################
atPickleFileName = 'costPickles/' + name + '.pickle'

#####################################################################################
#Reading avg temp data for the given region and adding to a list after preprocessing#
#####################################################################################
prices = list();
with open('costData/' + name + '.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        prices.append(row['price'])

atn = pickle.load( open(atPickleFileName, "rb" ) )

####################################################################################
#################          Predicting the avg temp result        ###################
####################################################################################
input = prices[len(prices) - 1]
print input
result = atn.activate(input)
print 'result ', result

