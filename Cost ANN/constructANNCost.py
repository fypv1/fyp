from pybrain.datasets import SupervisedDataSet
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer

from pybrain.datasets            import ClassificationDataSet
from pybrain.utilities           import percentError
from pybrain.tools.shortcuts     import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules   import SoftmaxLayer

from pylab import ion, ioff, figure, draw, contourf, clf, show, hold, plot
from scipy import diag, arange, meshgrid, where
from numpy.random import multivariate_normal
import sys, pickle, csv


atData = list()
pData = list()
name = sys.argv[1]


####################################################################################
######################            Pickle file name            ######################
####################################################################################
atPickleFileName = 'costPickles/' + name + '.pickle'
pPickleFileName = 'costPickles/' + name + '.pickle'



####################################################################################
######################            DATASET CREATION            ######################
####################################################################################
attrainingDataSet = SupervisedDataSet(1, 1)
attestingDataSet = SupervisedDataSet(1, 1)


####################################################################################
#####################            Opening csv files            ######################
####################################################################################


#####################################################################################
#Reading avg temp data for the given region and adding to a list after preprocessing#
#####################################################################################
prices = list();
with open('costData/' + name + '.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        prices.append(row['price'])

####################################################################################
######          Finding the training data size and testing data size        ########
####################################################################################
DataSize = len(prices) - 1
trainingDataSize = int((DataSize * 8) / 10);
testingDataSize = DataSize - trainingDataSize

####################################################################################
#######################          Training data set        ##########################
####################################################################################
for i in range(trainingDataSize):
    attrainingDataSet.addSample(prices[i], prices[i + 1])


####################################################################################
######################          Testing data set        ############################
####################################################################################
for i in range(trainingDataSize, DataSize):
    attestingDataSet.addSample(prices[i], prices[i + 1])


####################################################################################
##################          Builing network for avg temp        ####################
####################################################################################
atn = buildNetwork(attrainingDataSet.indim,1, 1, 1,recurrent=True)
att = BackpropTrainer(atn,learningrate=0.01,momentum=0.05,verbose=True)
att.trainOnDataset(attrainingDataSet,3000)
att.testOnData(attestingDataSet, verbose=True)

with open(atPickleFileName, 'wb') as handle:
    pickle.dump(atn, handle)



atn = pickle.load( open(atPickleFileName, "rb" ) )

####################################################################################
#################          Predicting the avg temp result        ###################
####################################################################################
input = prices[len(prices) - 1]
print input
result = atn.activate(input)
print 'result ', result

