import sys,os,pickle
from flask import Flask, render_template, request, redirect, send_from_directory
from fuzzyrecommendation import crop_recommendation
sys.path.insert(0,'EVALUATION/')
from eval_confuse_matrix import *

result = dict()
url = "index"
app = Flask(__name__)
@app.route("/evaluation",methods=['GET','POST'])
def evaluation():
    value = request.form.get('cost',0.5)
    if value == "maincrop":
        eval_metrics,confusion = evaluate_read(0.3,True)
    else:
        eval_metrics,confusion = evaluate_read(float(value),False)
    c = eval_metrics["c"]
    acc_score = eval_metrics["acc_score"]
    p = eval_metrics["p"]
    r = eval_metrics["r"]
    f1 = eval_metrics["f1"]
    
    c = c.split("\n")
    for i in range(len(c)):
        c[i] = c[i].split()

    del c[1]

    for i in range(len(c[0])):
        c[0][i] = c[0][i].capitalize()

    for i in range(len(c) - 3):
        c[i][0] = c[i][0].capitalize()

    for i in range(1,len(c) - 1):
        for j in range(len(c[i]) - 5):
            c[i][0] += " " + c[i][1]
            del c[i][1]

    c[0].insert(0, "Crop")

    for i in range(len(confusion[0])):
        confusion[0][i] = confusion[0][i].capitalize()

    for i in range(len(confusion)):
        confusion[i][0] = confusion[i][0].capitalize()
    
    return render_template('evaluation.html', c=c, acc_score = acc_score, p = p, r = r,f = f1, confusion = confusion)
@app.route('/suggest', methods = ['GET','POST'])
def suggest():
    region = request.form['region']
    n = -1
    p = -1
    k = -1
    year = 2016
    if(request.form['year'] != ""):
        year = int(request.form['year'])
    if(request.form['n'] != ""):
        n = float(request.form['n'])
    if(request.form['p'] != ""):
        p = float(request.form['p'])
    if(request.form['k'] != ""):
        k = float(request.form['k'])
    print n, p ,k
    url = "suggest"
    print("The region is '" + region + "'")
    result,weather = crop_recommendation(str(region),year,n,p,k,0.3)
    print result,weather
    
    month = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"]

    #result = '<br/>'.join(result.split('\n'))
    return render_template('index.html', result=result, weather = weather , month = month, url = url)
@app.route("/", methods = ['GET','POST'])
@app.route("/index",methods=['GET','POST'])
def main():
    return render_template('index.html')
if __name__ == "__main__":
	app.debug = True
	app.run(processes=1)
@app.route("/bootstrap.min.js", methods = ['GET','POST'])
def bootstrap():
    return render_template('bootstrap.min.js')
@app.route("/jumbotron-narrow.css", methods = ['GET','POST'])
def bootstrap2():
    return render_template('jumbotron-narrow.css')
